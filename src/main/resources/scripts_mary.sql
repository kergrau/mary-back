CREATE TABLE roles(
id INT PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(50));

CREATE TABLE users(
id INT PRIMARY KEY AUTO_INCREMENT,
identification_type VARCHAR(4) NOT NULL,
dni VARCHAR(10) UNIQUE NOT NULL,
name1 VARCHAR(40) NOT NULL,
name2 VARCHAR(40),
surname1 VARCHAR(40) NOT NULL,
surname2 VARCHAR(40),
address VARCHAR(100),
phone VARCHAR(10),
city VARCHAR(30),
username VARCHAR(50) NOT NULL,
email VARCHAR(50) NOT NULL,
password VARCHAR(120) NOT NULL);

CREATE TABLE user_roles(
user_id int,
role_id int,
FOREIGN KEY (user_id) REFERENCES users(id),
FOREIGN KEY (role_id) REFERENCES roles(id),
PRIMARY KEY(user_id, role_id)
);

CREATE TABLE user_financial_info(
id INT PRIMARY KEY,
rut VARCHAR(200) NOT NULL,
dni_support VARCHAR(200) NOT NULL,
sign VARCHAR(200) NOT NULL,
bank_reference VARCHAR(200) NOT NULL,
bank_account_type VARCHAR(2) NOT NULL,
bank_name VARCHAR(50) NOT NULL,
bank_account VARCHAR(12) NOT NULL,
FOREIGN KEY (id) REFERENCES users(id)
);

INSERT INTO roles(name) VALUES('ROLE_ADMIN');
INSERT INTO roles(name) VALUES('ROLE_USER');


CREATE TABLE invoices(
id INT PRIMARY KEY AUTO_INCREMENT,
generate_by int NOT NULL,
approved_by int,
created_at DATE,
approved_date DATE,
is_approved BOOLEAN,
FOREIGN KEY (generate_by) REFERENCES users(id),
FOREIGN KEY (approved_by) REFERENCES users(id)
);

CREATE TABLE invoice_details(
id INT PRIMARY KEY AUTO_INCREMENT,
invoice_id INT,
item VARCHAR(250) NOT NULL,
price DECIMAL NOT NULL,
FOREIGN KEY (invoice_id) REFERENCES invoices(id)
);
