package com.example.mary.dtos;

import java.util.Date;
import java.util.List;

import com.example.mary.entities.InvoiceDetail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceToApprovalDto {

	private Long id;
	private String generatedBy;
	private List<InvoiceDetail> invoiceDetails;
	private Date createDate;
	private String rut;
}
