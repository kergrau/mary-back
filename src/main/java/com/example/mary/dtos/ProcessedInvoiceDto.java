package com.example.mary.dtos;

import java.util.List;

import com.example.mary.entities.InvoiceDetail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProcessedInvoiceDto {

	private Long id;
	private String generatedBy;
	private String fullname; // fullname of generateBy
	private String approvedBy;
	private List<InvoiceDetail> invoiceDetails;
	private String createDate;
	private String approvedDate;
	private String rut;
	private String identificationType;
	private String dni;
	private String phone;
	private String email;
	private String bankAccountType;
	private String bankName;
	private String bankAccount;
	private String sign;
}
