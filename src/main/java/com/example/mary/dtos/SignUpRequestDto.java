package com.example.mary.dtos;

import java.util.Set;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignUpRequestDto {

	@NotBlank(message = "Campo no puede ir vacio")
	private String identificationType;
	@NotBlank(message = "Campo no puede ir vacio")
	private String dni;
	@NotBlank(message = "Campo no puede ir vacio")
	private String name1;
	private String name2;
	@NotBlank(message = "Campo no puede ir vacio")
	private String surname1;
	private String surname2;
	@NotBlank(message = "Campo no puede ir vacio")
	@Email
	private String email;
	private String address;
	private String phone;
	private String city;
	@NotBlank(message = "Campo no puede ir vacio")
	private String username;
	@NotBlank(message = "Campo no puede ir vacio")
	private String password;
	private Set<String> roles;
	@Valid
	private UserFinancialInfoDto userFinancialInfo;
}
