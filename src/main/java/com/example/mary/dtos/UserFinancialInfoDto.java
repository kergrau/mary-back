package com.example.mary.dtos;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserFinancialInfoDto {

	@NotBlank(message = "Campo no puede ir vacio")
	private String rut;
	@NotBlank(message = "Campo no puede ir vacio")
	private String dniSupport;
	@NotBlank(message = "Campo no puede ir vacio")
	private String sign;
	@NotBlank(message = "Campo no puede ir vacio")
	private String bankReference;
	@NotBlank(message = "Campo no puede ir vacio")
	private String bankAccountType;
	@NotBlank(message = "Campo no puede ir vacio")
	private String bankName;
	@NotBlank(message = "Campo no puede ir vacio")
	private String bankAccount;
}
