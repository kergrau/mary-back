package com.example.mary.services;

import java.util.List;

import com.example.mary.dtos.InvoiceToApprovalDto;
import com.example.mary.dtos.ProcessedInvoiceDto;
import com.example.mary.entities.Invoice;
import com.example.mary.entities.InvoiceDetail;

public interface InvoiceService {

	List<InvoiceToApprovalDto> listPendingInvoices();

	void approvedInvoice(Long id, String token, boolean decision);

	Invoice save(List<InvoiceDetail> items, String token);

	List<ProcessedInvoiceDto> processedInvoices();

	List<ProcessedInvoiceDto> processedInvoicesByUser(String token);
}
