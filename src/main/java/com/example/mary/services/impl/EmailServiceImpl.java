package com.example.mary.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.example.mary.services.EmailService;

@Service
public class EmailServiceImpl implements EmailService {

	@Autowired
	private JavaMailSender mailSender;

	@Async
	@Override
	public void sendProcessedMail(Long id, String to, boolean isApproved) {
		SimpleMailMessage message = new SimpleMailMessage();
		String body = new StringBuilder().append("You invoice number ").append(id).append(" was ")
				.append(isApproved ? "approved." : "rejected.").toString();
		String subject = "Invoice Processed";
		message.setFrom("maryapp10@outlook.com");
		message.setTo(to);
		message.setSubject(subject);
		message.setText(body);

		mailSender.send(message);

	}

}
