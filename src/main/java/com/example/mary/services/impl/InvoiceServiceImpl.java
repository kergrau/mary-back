package com.example.mary.services.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.example.mary.dtos.InvoiceToApprovalDto;
import com.example.mary.dtos.ProcessedInvoiceDto;
import com.example.mary.entities.Invoice;
import com.example.mary.entities.InvoiceDetail;
import com.example.mary.entities.User;
import com.example.mary.enums.ECustomError;
import com.example.mary.exceptions.CustomException;
import com.example.mary.repositories.InvoiceRepository;
import com.example.mary.repositories.UserRepository;
import com.example.mary.security.jwt.JwtUtils;
import com.example.mary.services.EmailService;
import com.example.mary.services.InvoiceService;

@Service
public class InvoiceServiceImpl implements InvoiceService {

	@Autowired
	private InvoiceRepository invoiceRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private EmailService emailService;

	@Autowired
	private JwtUtils jwtUtils;

	@Override
	public List<InvoiceToApprovalDto> listPendingInvoices() {
		List<Invoice> invoices = invoiceRepository.findByIsApproved(null);
		return invoices.stream()
				.map(invoice -> new InvoiceToApprovalDto(invoice.getId(), invoice.getGenerateBy().getUsername(),
						invoice.getInvoiceDetails(), invoice.getCreatedAt(),
						invoice.getGenerateBy().getUserFinancialInfo().getRut()))
				.toList();
	}

	@Override
	public List<ProcessedInvoiceDto> processedInvoices() {
		List<Invoice> invoices = invoiceRepository.findByIsApprovedNotNull();
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		return invoices.stream()
				.map(invoice -> new ProcessedInvoiceDto(invoice.getId(), invoice.getGenerateBy().getUsername(),
						invoice.getGenerateBy().getName1() + " " + invoice.getGenerateBy().getSurname1(),
						invoice.getApprovedBy().getUsername(), invoice.getInvoiceDetails(),
						simpleDateFormat.format(invoice.getCreatedAt()),
						simpleDateFormat.format(invoice.getApprovedDate()),
						invoice.getGenerateBy().getUserFinancialInfo().getRut(),
						invoice.getGenerateBy().getIdentificationType(), invoice.getGenerateBy().getDni(),
						invoice.getGenerateBy().getPhone(), invoice.getGenerateBy().getEmail(),
						invoice.getGenerateBy().getUserFinancialInfo().getBankAccountType(),
						invoice.getGenerateBy().getUserFinancialInfo().getBankName(),
						invoice.getGenerateBy().getUserFinancialInfo().getBankAccount(),
						invoice.getGenerateBy().getUserFinancialInfo().getSign()))
				.toList();
	}

	@Override
	public List<ProcessedInvoiceDto> processedInvoicesByUser(String token) {
		User user = getUser(token);
		List<Invoice> invoices = invoiceRepository.findByIsApprovedNotNullAndGenerateBy(user);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		return invoices.stream()
				.map(invoice -> new ProcessedInvoiceDto(invoice.getId(), invoice.getGenerateBy().getUsername(),
						invoice.getGenerateBy().getName1() + " " + invoice.getGenerateBy().getSurname1(),
						invoice.getApprovedBy().getUsername(), invoice.getInvoiceDetails(),
						simpleDateFormat.format(invoice.getCreatedAt()),
						simpleDateFormat.format(invoice.getApprovedDate()),
						invoice.getGenerateBy().getUserFinancialInfo().getRut(),
						invoice.getGenerateBy().getIdentificationType(), invoice.getGenerateBy().getDni(),
						invoice.getGenerateBy().getPhone(), invoice.getGenerateBy().getEmail(),
						invoice.getGenerateBy().getUserFinancialInfo().getBankAccountType(),
						invoice.getGenerateBy().getUserFinancialInfo().getBankName(),
						invoice.getGenerateBy().getUserFinancialInfo().getBankAccount(),
						invoice.getGenerateBy().getUserFinancialInfo().getSign()))
				.toList();
	}

	@Override
	public void approvedInvoice(Long id, String token, boolean decision) {
		Invoice invoice = invoiceRepository.findById(id)
				.orElseThrow(() -> new CustomException(ECustomError.INVOICE_NOT_FOUND, HttpStatus.BAD_REQUEST));
		if (invoice.getApprovedBy() == null) {
			User user = getUser(token);
			invoice.setApprovedBy(user);
			invoice.setApprovedDate(new Date());
			invoice.setIsApproved(decision);
			invoiceRepository.save(invoice);
			emailService.sendProcessedMail(id, invoice.getGenerateBy().getEmail(), decision);
		} else {
			throw new CustomException(ECustomError.INVOICE_APPROVED, HttpStatus.CONFLICT);
		}
	}

	@Override
	public Invoice save(List<InvoiceDetail> items, String token) {
		Invoice invoice = new Invoice();
		User user = getUser(token);
		invoice.setGenerateBy(user);
		invoice.setInvoiceDetails(items);
		Invoice invoiceSaved = invoiceRepository.save(invoice);
		return invoiceSaved;
	}

	private User getUser(String token) {
		token = jwtUtils.extractToken(token);
		String username = jwtUtils.getUserNameFromJwtToken(token);
		return userRepository.findByUsername(username)
				.orElseThrow(() -> new CustomException(ECustomError.PERSON_NOT_FOUND, HttpStatus.NOT_FOUND));
	}

}
