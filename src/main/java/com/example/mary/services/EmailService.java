package com.example.mary.services;

public interface EmailService {

	void sendProcessedMail(Long id, String to, boolean isApproved);
}
