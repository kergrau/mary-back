package com.example.mary.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.mary.entities.Invoice;
import com.example.mary.entities.User;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

	List<Invoice> findByIsApproved(Boolean isApproved);

	List<Invoice> findByIsApprovedNotNull();

	List<Invoice> findByIsApprovedNotNullAndGenerateBy(User user);

}
