package com.example.mary.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.mary.entities.UserFinancialInfo;

public interface UserFinancialInfoRepository extends JpaRepository<UserFinancialInfo, Long> {

}
