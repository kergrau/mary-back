package com.example.mary.entities;

import org.springframework.beans.BeanUtils;

import com.example.mary.dtos.UserFinancialInfoDto;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "user_financial_info")
public class UserFinancialInfo {

	@Id
	private Long id;
	@Column
	private String rut;
	@Column(name = "dni_support")
	private String dniSupport;
	@Column
	private String sign;
	@Column(name = "bank_reference")
	private String bankReference;
	@Column(name = "bank_account_type")
	private String bankAccountType;
	@Column(name = "bank_name")
	private String bankName;
	@Column(name = "bank_account")
	private String bankAccount;

	public UserFinancialInfo toEntity(UserFinancialInfoDto user, Long id) {
		UserFinancialInfo userEntity = new UserFinancialInfo();
		BeanUtils.copyProperties(user, userEntity);
		userEntity.setId(id);
		return userEntity;
	}
}
