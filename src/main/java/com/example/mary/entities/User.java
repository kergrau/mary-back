package com.example.mary.entities;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.BeanUtils;

import com.example.mary.dtos.SignUpRequestDto;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "users")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "identification_type")
	private String identificationType;

	@Column
	private String dni;

	@Column
	private String name1;

	@Column
	private String name2;

	@Column
	private String surname1;

	@Column
	private String surname2;

	@Column
	private String address;

	@Column
	private String phone;

	@Column
	private String city;

	@Column
	private String username;

	@Column
	private String email;

	@Column
	private String password;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles = new HashSet<>();

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id", referencedColumnName = "id")
	private UserFinancialInfo userFinancialInfo;
	
	public User toEntity(SignUpRequestDto user) {
		User userEntity = new User();
		BeanUtils.copyProperties(user, userEntity);
		return userEntity;
	}
}
