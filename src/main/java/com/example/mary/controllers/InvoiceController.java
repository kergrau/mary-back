package com.example.mary.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.mary.dtos.InvoiceToApprovalDto;
import com.example.mary.dtos.ProcessedInvoiceDto;
import com.example.mary.entities.InvoiceDetail;
import com.example.mary.services.InvoiceService;

@RestController
@RequestMapping("/api/invoice")
public class InvoiceController {

	@Autowired
	private InvoiceService invoiceService;

	@PostMapping("/save")
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<?> save(@RequestBody List<InvoiceDetail> items,
			@RequestHeader(name = "Authorization") String token) {
		invoiceService.save(items, token);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@GetMapping("/list-pending")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> listPending() {
		List<InvoiceToApprovalDto> invoices = invoiceService.listPendingInvoices();
		return new ResponseEntity<>(invoices, HttpStatus.OK);
	}

	@GetMapping("/list-processed")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> listProcessed() {
		List<ProcessedInvoiceDto> invoices = invoiceService.processedInvoices();
		return new ResponseEntity<>(invoices, HttpStatus.OK);

	}

	@GetMapping("/list-processed-by-user")
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<?> listProcessedByUser(@RequestHeader(name = "Authorization") String token) {
		List<ProcessedInvoiceDto> invoices = invoiceService.processedInvoicesByUser(token);
		return new ResponseEntity<>(invoices, HttpStatus.OK);

	}

	@PatchMapping("/approve")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> approve(@RequestHeader(name = "Authorization") String token,
			@RequestParam(name = "id") Long id, @RequestParam boolean decision) {
		invoiceService.approvedInvoice(id, token, decision);
		return new ResponseEntity<>(HttpStatus.OK);

	}
}
